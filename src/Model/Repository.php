<?php

namespace App\Model;

use App\Component\ConfigProvider\ConfigProvider;
use App\Component\Db\DbConnection;
use App\Component\Db\DbConnectionFactory;
use App\Component\Db\Exception\ConfigNotExistsException;

class Repository
{
    private DbConnection $dbConnection;

    /**
     * @throws ConfigNotExistsException
     */
    public function __construct(private readonly DbConnectionFactory $dbConnectionFactory)
    {
        /**
         * Pokiaľ by existovala DB, odkkomentujeme
         */
//        $this->dbConnection = ($this->dbConnectionFactory)(
//            ConfigProvider::getConfig('db_server'),
//            ConfigProvider::getConfig('db_username'),
//            ConfigProvider::getConfig('db_password'),
//            ConfigProvider::getConfig('db_name')
//        );
    }

    public function getDbConnection(): DbConnection
    {
        return $this->dbConnection;
    }

    public function prepare(string $sql): \mysqli_stmt
    {
        return $this->getDbConnection()->connection()->prepare($sql);
    }

    public function bindParams(\mysqli_stmt $stmt, string $types, array $params): \mysqli_stmt
    {
        $stmt->bind_param($types, ...$params);

        return $stmt;
    }

    public function execute(string $sql, string $types, array $params): void
    {
        $stmt = $this->prepare($sql);
        $this->bindParams($stmt, $types, $params);
        $stmt->execute();
        $stmt->close();
    }

    public function query(string $sql): \mysqli_result
    {
        return $this->getDbConnection()->connection()->query($sql);
    }
}
