<?php

namespace App\Model\Film;

class FilmFacade
{
    public function __construct(private readonly FilmRepository $filmRepository)
    {
    }

    public function getById(int $id): Film
    {
        return $this->filmRepository->getById($id);
    }

    public function create(Film $film): Film
    {
        return $this->filmRepository->create($film);
    }
}
