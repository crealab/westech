<?php

namespace App\Model\Film\Exception;

class RequestDataNotValidException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Not valid request data.');
    }
}
