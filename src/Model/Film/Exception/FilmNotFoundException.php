<?php

namespace App\Model\Film\Exception;

class FilmNotFoundException extends \Exception
{
    public function __construct(int $id)
    {
        parent::__construct(sprintf('No film found for ID %s.', $id));
    }
}
