<?php

namespace App\Model\Film\Exception;

class NotValidRatingValueException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Not valid rating value.');
    }
}
