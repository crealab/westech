<?php

namespace App\Model\Film;

use App\Model\Film\Exception\NotValidRatingValueException;

/**
 * V prípade ORM by boli nastavené Constraints (NotBlank v prípade povinných polí), id ako identity a autoincrement, prípadne anotácie pre api-platform (definovanie endpointov, controllerov ...)
 */
class Film
{
    private int $id;
    private string $name;
    private int $year;
    private string $genre;
    private string $director;
    private string $cast;
    private string $description;
    private int $rating;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Film
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Film
    {
        $this->name = $name;

        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): Film
    {
        $this->year = $year;

        return $this;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): Film
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDirector(): string
    {
        return $this->director;
    }

    public function setDirector(string $director): Film
    {
        $this->director = $director;

        return $this;
    }

    public function getCast(): string
    {
        return $this->cast;
    }

    public function setCast(string $cast): Film
    {
        $this->cast = $cast;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): Film
    {
        $this->description = $description;

        return $this;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @throws NotValidRatingValueException
     */
    public function setRating(int $rating): Film
    {
        if ($rating >= 0 && $rating <= 10) {
            $this->rating = $rating;
        } else {
            throw new NotValidRatingValueException();
        }

        return $this;
    }
}
