<?php

namespace App\Model\Film\UseCase;

use App\Model\Film\Exception\NotValidRatingValueException;
use App\Model\Film\Exception\RequestDataNotValidException;
use App\Model\Film\OperationStep\PrepareNewFilmFromRequestStep;
use App\Model\Film\OperationStep\SaveNewFilmStep;

class CreateNewFilmUseCase
{
    public function __construct(
        private readonly PrepareNewFilmFromRequestStep $prepareNewFilmFromRequestStep,
        private readonly SaveNewFilmStep $saveNewFilmStep
    )
    {
    }

    /**
     * @throws RequestDataNotValidException
     * @throws NotValidRatingValueException
     */
    public function __invoke(array $data)
    {
        // 1. Prepare new film
        $film = ($this->prepareNewFilmFromRequestStep)($data);
        // 2. Save film
        return ($this->saveNewFilmStep)($film);
    }
}
