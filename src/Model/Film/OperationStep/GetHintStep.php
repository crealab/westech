<?php

namespace App\Model\Film\OperationStep;

use App\Component\TheMovieDb\ApiRest;
use App\Component\TheMovieDb\Exception\RequestException;

class GetHintStep
{
    public function __construct(private readonly ApiRest $apiRest)
    {

    }

    /**
     * @throws RequestException
     */
    public function __invoke(string $name): array
    {
        return ($this->apiRest)->getFilmByName($name);
    }
}
