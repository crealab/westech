<?php

namespace App\Model\Film\OperationStep;

use App\Model\Film\Exception\RequestDataNotValidException;
use App\Model\Film\Film;
use App\Model\Film\FilmFacade;

class SaveNewFilmStep
{
    public function __construct(private readonly FilmFacade $filmFacade)
    {
    }

    public function __invoke(Film $film): Film
    {
        return $this->filmFacade->create($film);
    }
}
