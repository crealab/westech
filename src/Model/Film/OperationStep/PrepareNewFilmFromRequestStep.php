<?php

namespace App\Model\Film\OperationStep;

use App\Model\Film\Exception\NotValidRatingValueException;
use App\Model\Film\Exception\RequestDataNotValidException;
use App\Model\Film\Film;

class PrepareNewFilmFromRequestStep
{
    /**
     * @throws RequestDataNotValidException
     * @throws NotValidRatingValueException
     */
    public function __invoke(array $data): Film
    {
        $name = htmlspecialchars($data['name']) ?? null;
        $year = (int) htmlspecialchars($data['year']) ?? null;
        $genre = htmlspecialchars($data['genre']) ?? null;
        $director = htmlspecialchars($data['director']) ?? null;
        $cast = htmlspecialchars($data['cast']) ?? null;
        $description =htmlspecialchars( $data['description']) ?? null;
        $rating = (int) htmlspecialchars($data['rating']) ?? null;

        if (
            null === $name ||
            null === $year ||
            null === $genre ||
            null === $director ||
            null === $cast ||
            null === $description ||
            null === $rating
        ) {
            throw new RequestDataNotValidException();
        }

        $film = new Film();

        return $film
            ->setName($name)
            ->setYear($year)
            ->setGenre($genre)
            ->setDirector($director)
            ->setCast($cast)
            ->setDescription($description)
            ->setRating($rating);
    }
}
