<?php

namespace App\Model\Film;

use App\Model\Film\Exception\FilmNotFoundException;
use App\Model\Repository;

/**
 * V ideálnom prípade implementácia ORM
 */
class FilmRepository extends Repository
{
    public function getById(int $id): Film
    {
        /**
         *  Pokiaľ by existovalo pripojenie na DB využijeme select
         */
//        $results = $this->query('SELECT * FROM filmy WHERE id = '.$id);
//
//        if ($results->num_rows === 0) {
//            throw new FilmNotFoundException($id);
//        }
//
//        $result = $results->num_rows[0];

        $result = [
            'name' => 'Tajná invázia',
            'year' => 2023,
            'genre' => 'Akčný a Dobrodružný, Dráma, Sci-Fi & Fantasy',
            'director' => 'Jonh Doe',
            'cast' => 'Jonh Doe',
            'description' => 'Nick Fury and Talos discover a faction of shapeshifting Skrulls who have been infiltrating Earth for years.',
            'rating' => 8,
        ];

        $film = new Film();
        $film
            ->setId($id)
            ->setName($result['name'])
            ->setYear((int) $result['year'])
            ->setGenre($result['genre'])
            ->setDirector($result['director'])
            ->setCast($result['cast'])
            ->setDescription($result['description'])
            ->setRating((int) $result['rating']);

        return $film;
    }
    public function create(Film $film): Film
    {
        /**
         *  Pokiaľ by existovalo pripojenie na DB využijeme insert
         */
//        $this->execute('INSERT INTO filmy (name, year, genre, director, cast, description, rating) VALUES (?, ?, ?, ?, ?, ?, ?)',
//            'sissssi',
//            [
//                $film->getName(),
//                $film->getYear(),
//                $film->getGenre(),
//                $film->getDirector(),
//                $film->getCast(),
//                $film->getDescription(),
//                $film->getRating()
//            ]
//        );

        $film->setId(1);

        return $film;
    }
}
