<?php

namespace App\Component\ConfigProvider;

use App\Component\Db\Exception\ConfigNotExistsException;

class ConfigProvider
{
    public const CONFIG = [
        'db_server' => 'localhost',
        'db_username' => 'root',
        'db_password' => 'root',
        'db_name' => 'themoviedb',
        'themoviedb_api_key' => '',
    ];

    /**
     * @throws ConfigNotExistsException
     */
    public static function getConfig(string $name): string
    {
        if (false === isset(self::CONFIG[$name])) {
            throw new ConfigNotExistsException($name);
        }

        return self::CONFIG[$name];
    }
}
