<?php

namespace App\Component\Db\Exception;

class ConfigNotExistsException extends \Exception
{
    public function __construct(string $name)
    {
        parent::__construct(sprintf('Config with name %s not exists', $name));
    }
}
