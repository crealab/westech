<?php

namespace App\Component\Db;

use App\Component\Db\Exception\ConnectionException;

class DbConnectionFactory
{
    public function __invoke(
        string $server,
        string $username,
        string $password,
        string $database
    ): DbConnection
    {
        return new DbConnection($server, $username, $password, $database);
    }
}
