<?php

namespace App\Component\Db;

use App\Component\Db\Exception\ConnectionException;

class DbConnection
{
    private \mysqli $connection;

    /**
     * @throws ConnectionException
     */
    public function __construct(
        private readonly string $server,
        private readonly string $username,
        private readonly string $password,
        private readonly string $database
    )
    {
        $this->connection = new \mysqli($this->server, $this->username, $this->password, $this->database);

        if ($this->connection->connect_error) {
            throw new ConnectionException($this->connection->error);
        }
    }

    public function connection(): \mysqli
    {
        return $this->connection;
    }
}
