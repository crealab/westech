<?php

namespace App\Component\Db\Exception;

class ConnectionException extends \Exception
{
    public function __construct(string $detail)
    {
        parent::__construct(sprintf('Problem with connection to database. Error: %s', $detail));
    }
}
