<?php

namespace App\Component\TheMovieDb\Exception;

class RequestException extends \Exception
{
    public function __construct(string $detail)
    {
        parent::__construct(sprintf('Problem with connection to api. Error: %s', $detail));
    }
}
