<?php

namespace App\Component\TheMovieDb;

use App\Component\TheMovieDb\Exception\RequestException;

class ApiRest
{
    private const BASE_URL = 'https://api.themoviedb.org/3/';

    private string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    private function getRequestUrl(string $endpoint, array $parameters): string
    {
        return self::BASE_URL . $endpoint .'?'. http_build_query(array_merge(['api_key' => $this->apiKey, $parameters]));
    }

    /**
     * @return Response[]
     * @throws RequestException
     */
    private function request(string $url): array
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new RequestException($err);
        }

        $response = json_decode($response, true);

        if (false === $response['success']) {
            $response['results'] = [];
        }

        return $response;
    }

    private function response(array $data): array
    {
        $result = [];

        if (false === empty($data['results'])) {
            foreach ($data['results'] as $film) {
                $result[] = new Response(
                    $film['title'],
                    $film['release_date'],
                    $film['genre_ids'],
                    $film['director'],
                    $film['cast'],
                    $film['overview'],
                    $film['vote_average']
                );
            }
        }

        return $result;
    }

    /**
     * @throws RequestException
     */
    public function getFilmByGenre(string $genre): array
    {
        $response = $this->request($this->getRequestUrl('discover/movie', ['with_genres' => $genre]));
        return $this->response($response);
    }

    /**
     * @throws RequestException
     */
    public function getFilmByYear(string $year): array
    {
        $response = $this->request($this->getRequestUrl('discover/movie', ['primary_release_year' => $year, 'sort_by' => 'popularity.desc']));
        return $this->response($response);
    }

    /**
     * @throws RequestException
     */
    public function getFilmByDirector(string $director): array
    {
        $response = $this->request($this->getRequestUrl('search/person', ['query' => $director]));

        if (null === $director = $response['results'][0]['id'] ?? null) {
            $this->response([]);
        }

        $response = $this->request($this->getRequestUrl('discover/movie', ['with_crew' => $director, 'sort_by' => 'popularity.desc']));
        return $this->response($response);
    }

    /**
     * @throws RequestException
     */
    public function getFilmByCast(string $cast): array
    {
        $response = $this->request($this->getRequestUrl('search/person', ['query' => $cast]));

        if (null === $cast = $response['results'][0]['id'] ?? null) {
            $this->response([]);
        }

        $response = $this->request($this->getRequestUrl('discover/movie', ['with_cast' => $cast, 'sort_by' => 'popularity.desc']));
        return $this->response($response);
    }

    /**
     * @throws RequestException
     */
    public function getFilmByName(string $name, int $count = 5): array
    {
        $response = $this->request($this->getRequestUrl('search/movie', ['query' => $name]));

        $total = min($count, count($response['results']));
        $relevantFilms = array_slice((array) $response['results'], 0, $total);

        return $this->response($relevantFilms);
    }
}
