<?php

namespace App\Component\TheMovieDb;

class Response
{
    public function __construct(
        private readonly string $name,
        private readonly string $year,
        private readonly string $genre,
        private readonly string $director,
        private readonly string $cast,
        private readonly string $description,
        private readonly string $rating,
    )
    {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getYear(): string
    {
        return $this->year;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getDirector(): string
    {
        return $this->director;
    }

    public function getCast(): string
    {
        return $this->cast;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getRating(): string
    {
        return $this->rating;
    }
}
