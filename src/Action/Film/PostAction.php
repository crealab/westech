<?php

namespace App\Action\Film;

use App\Model\Film\Exception\NotValidRatingValueException;
use App\Model\Film\Exception\RequestDataNotValidException;
use App\Model\Film\Film;
use App\Model\Film\UseCase\CreateNewFilmUseCase;

class PostAction
{
    public function __construct(private readonly CreateNewFilmUseCase $createNewFilmUseCase)
    {
    }

    /**
     * @throws NotValidRatingValueException
     * @throws RequestDataNotValidException
     */
    public function __invoke(array $data): Film
    {
        return ($this->createNewFilmUseCase)($data);
    }
}
