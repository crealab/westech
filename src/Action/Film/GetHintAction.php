<?php

namespace App\Action\Film;

use App\Model\Film\Film;
use App\Model\Film\UseCase\CreateNewFilmUseCase;

class GetHintAction
{
    public function __construct(private readonly CreateNewFilmUseCase $createNewFilmUseCase)
    {
    }

    public function __invoke(array $data): Film
    {
        return ($this->createNewFilmUseCase)($data);
    }
}
