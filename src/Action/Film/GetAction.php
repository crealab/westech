<?php

namespace App\Action\Film;

use App\Model\Film\Film;
use App\Model\Film\FilmFacade;
use App\Model\Film\UseCase\CreateNewFilmUseCase;

class GetAction
{
    public function __construct(private readonly FilmFacade $filmFacade)
    {
    }

    public function __invoke(int $id): Film
    {
        return $this->filmFacade->getById($id);
    }
}
