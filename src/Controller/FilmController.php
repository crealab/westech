<?php

namespace App\Controller;

use App\Action\Film\PostAction;

class FilmController
{
    public function __construct(private readonly PostAction $postAction)
    {
    }

    public function indexAction()
    {
        if (isset($_POST['submitted'])) {
            $film = ($this->postAction)($_POST);
        }


    }
}
