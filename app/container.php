<?php

use App\Action\Film\GetAction;
use App\Action\Film\GetHintAction;
use App\Action\Film\PostAction;
use App\Component\ConfigProvider\ConfigProvider;
use App\Component\Db\DbConnectionFactory;
use App\Component\TheMovieDb\ApiRest;
use App\Model\Film\FilmFacade;
use App\Model\Film\FilmRepository;
use App\Model\Film\OperationStep\GetHintStep;
use App\Model\Film\OperationStep\PrepareNewFilmFromRequestStep;
use App\Model\Film\OperationStep\SaveNewFilmStep;
use App\Model\Film\UseCase\CreateNewFilmUseCase;
use App\Model\Repository;
use DI\ContainerBuilder;
use function DI\create;
use function DI\get;

require_once dirname(dirname(__DIR__)) . '/westech/vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(false);
$containerBuilder->addDefinitions([
    ConfigProvider::class => create(ConfigProvider::class)->constructor(),
    DbConnectionFactory::class => create(DbConnectionFactory::class)->constructor(ConfigProvider::getConfig('db_server'), ConfigProvider::getConfig('db_username'), ConfigProvider::getConfig('db_password'), ConfigProvider::getConfig('db_name')),
    Repository::class => create(Repository::class),
    FilmFacade::class => create(FilmFacade::class)->constructor(get(FilmRepository::class)),
    FilmRepository::class => create(FilmRepository::class)->constructor(get(DbConnectionFactory::class)),
    CreateNewFilmUseCase::class => create(CreateNewFilmUseCase::class)->constructor(get(PrepareNewFilmFromRequestStep::class), get(SaveNewFilmStep::class)),
    PrepareNewFilmFromRequestStep::class => create(PrepareNewFilmFromRequestStep::class),
    SaveNewFilmStep::class => create(SaveNewFilmStep::class)->constructor(get(FilmFacade::class)),
    ApiRest::class => create(ApiRest::class)->constructor(ConfigProvider::getConfig('themoviedb_api_key')),
    PostAction::class => create(PostAction::class)->constructor(get(CreateNewFilmUseCase::class)),
    GetAction::class => create(GetAction::class)->constructor(get(FilmFacade::class)),
    GetHintStep::class => create(GetHintStep::class)->constructor(get(ApiRest::class)),
    GetHintAction::class => create(GetHintAction::class)->constructor(get(GetHintStep::class)),
]);

global $container;
$container = $containerBuilder->build();
