<?php

declare(strict_types=1);

use App\Component\TheMovieDb\Response;
use App\Model\Film\OperationStep\GetHintStep;

require_once dirname(__DIR__) . '/westech/app/container.php';

global $container;

// nájdi relevantné filmy, zakomentované api kľúč nie je definovaný
//try {
//    $films = $container->get(GetHintStep::class)($_GET['name']);
//} catch (Exception $e) {
//    $films = [];
//}

// demo data, api kľúč nie je definovaný
$films = [
    new Response('Through My Window: Across the Sea','2023','Romantický, Dráma, Komédia','Marçal Forés','Ariana Godoy', 'After a year of long-distance, Raquel and Ares reunite on a steamy beach trip. Faced with fresh flirtations and insecurities, will their love prevail?', '7'),
    new Response('Through My Window: Across the Sea','2023','Romantický, Dráma, Komédia','Marçal Forés','Ariana Godoy', 'After a year of long-distance, Raquel and Ares reunite on a steamy beach trip. Faced with fresh flirtations and insecurities, will their love prevail?', '7'),
    new Response('Through My Window: Across the Sea','2023','Romantický, Dráma, Komédia','Marçal Forés','Ariana Godoy', 'After a year of long-distance, Raquel and Ares reunite on a steamy beach trip. Faced with fresh flirtations and insecurities, will their love prevail?', '7'),
    new Response('Through My Window: Across the Sea','2023','Romantický, Dráma, Komédia','Marçal Forés','Ariana Godoy', 'After a year of long-distance, Raquel and Ares reunite on a steamy beach trip. Faced with fresh flirtations and insecurities, will their love prevail?', '7'),
    new Response('Through My Window: Across the Sea','2023','Romantický, Dráma, Komédia','Marçal Forés','Ariana Godoy', 'After a year of long-distance, Raquel and Ares reunite on a steamy beach trip. Faced with fresh flirtations and insecurities, will their love prevail?', '7'),
];

?>

<?php if (count($films) > 0) { ?>
    <?php foreach ($films as $film) { ?>
        <div class="hint-film"
             data-film="<?php echo htmlspecialchars(json_encode([
                 'name' => $film->getName(),
                 'year' => $film->getYear(),
                 'genre' => $film->getGenre(),
                 'director' => $film->getDirector(),
                 'cast' => $film->getCast(),
                 'description' => $film->getDescription(),
                 'rating' => $film->getRating(),
             ])) ?>"
        >
            <?php echo $film->getName() ?>
        </div>
    <?php } ?>
<?php } ?>

<script type="text/javascript">
    $(function() {
        $('.hint-film').click(function ()  {
            var data = $(this).data('film')
            console.log(data)
            $('input[name="name"]').val(data.name);
            $('input[name="year"]').val(data.year);
            $('input[name="genre"]').val(data.genre);
            $('input[name="director"]').val(data.director);
            $('input[name="cast"]').val(data.cast);
            $('input[name="description"]').val(data.description);
            $('input[name="rating"]').val(data.rating);
            $('#search_results').hide();
        })
    })
</script>
