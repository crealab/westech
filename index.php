<?php

// tento typ vykreslenia rozhrania bol pouzity len v ramci minimalizacie 3rd party knihovien,
// v idealnom pripade by bolo využitý celý mvc pattern s renderingom z twig šablóny respektive vue + api
// formulár ako samostatný prvok, v prípade Symfony by bol využitý Symfony FormType, šablóna rozčlenená na základný layout a obsah konkrétnej stránky

declare(strict_types=1);

use App\Action\Film\GetAction;
use App\Action\Film\PostAction;
use App\Component\TheMovieDb\ApiRest;

require_once dirname(__DIR__) . '/westech/app/container.php';

global $container;

// toto za bežných okolností by bolo v controlleri, respektíve by to handlovala iná knižnica, napríklad api-platform v spojení s vue
$film = null;

if (isset($_POST['submitted'])) {
    $film  = $container->get(PostAction::class)($_POST);
}

if (isset($_GET['id'])) {
    $film  = $container->get(GetAction::class)((int) $_GET['id']);
}

// odporúčané filmy, zakomentovane, api kľúč nie je definovaný
//if (null !== $film) {
//    $filmsByDirector = $container->get(ApiRest::class)->getFilmByDirector($film->getDirector());
//    $filmsByCast = $container->get(ApiRest::class)->getFilmByCast($film->getCast());
//    $filmsByGenre = $container->get(ApiRest::class)->getFilmByGenre($film->getGenre());
//    $filmsByYear = $container->get(ApiRest::class)->getFilmByYear((string) $film->getYear());
//}

?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo null === $film ? "Pridať film" : "Upraviť film" ?></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <style>
            .hints {
                display: none;
                position: absolute;
                top: 110px;
                background: white;
                padding: 10px;
                box-shadow: 0 5px 5px rgba(0,0,0,.15);
            }>
            .hints.show {
                display: block;
            }
            .hint-film {
                cursor: pointer;
            }
            .hint-film:hover {
                color: green;
            }
            .hint-film:not(:last-child) {
                border-bottom: 1px solid #dedede;
                margin-bottom: 5px;
                padding-bottom: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <h2><?php echo null === $film ? "Pridať film" : "Upraviť film ".$film->getName() ?></h2>

                    <div class="hints" id="search_results"></div>

                    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <div class="form-group">
                            <label for="name">Názov:</label>
                            <input class="form-control" type="text" name="name" required value="<?php echo $film?->getName(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="year">Rok vydania:</label>
                            <input class="form-control" type="text" name="year" required value="<?php echo $film?->getYear(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="genre">Žáner:</label>
                            <input class="form-control" type="text" name="genre" required value="<?php echo $film?->getGenre(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="director">Režisér:</label>
                            <input class="form-control" type="text" name="director" required value="<?php echo $film?->getDirector(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="cast">Herecké obsadenie:</label>
                            <input class="form-control" type="text" name="cast" required value="<?php echo $film?->getCast(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="description">Popis:</label>
                            <input class="form-control" type="text" name="description" required value="<?php echo $film?->getDescription(); ?>">
                        </div>

                        <div class="form-group">
                            <label for="rating">Hodnotenie (0-10):</label>
                            <input class="form-control" type="number" name="rating" min="0" max="10" required value="<?php echo $film?->getRating(); ?>">
                        </div>

                        <br>

                        <div class="form-group">
                            <input type="hidden" name="submitted" value="1">
                            <input class="btn btn-primary btn-lg btn-block" type="submit" value="Pridať film">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

        <script type="text/javascript">
            $(function() {
                $('input[name="name"]').keyup(function ()  {
                    if($('input[name="name"]').val().length > 3) {
                        $.ajax({
                            type: "GET",
                            url: "hint.php",
                            data: 'name=' + $(this).val(),
                            success: function(responseHtml) {
                                $('#search_results').html(responseHtml).show();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.error(jqXHR, textStatus, errorThrown)
                            }
                        });
                    } else {
                        $('#search_results').hide();
                    }
                })
            })
        </script>
    </body>
</html>
